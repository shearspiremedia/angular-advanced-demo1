import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Car } from "../../models/car.model";

@Component({
  selector: "app-car-table",
  templateUrl: "./car-table.component.html",
  styleUrls: ["./car-table.component.css"]
})
export class CarTableComponent implements OnInit {
  @Input() sortAscending = true;
  @Input() sortedCars: Car[];
  @Input() sortColName = 'id';
  @Input() editCarId = -1;
  @Output() deleteRow = new EventEmitter<number>();
  @Output() sortCol = new EventEmitter<string>();
  @Output() editCar = new EventEmitter<number>();
  @Output() saveCar = new EventEmitter<Car>();
  @Output() cancelCar = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  doEditRow(id: number) {
    this.editCar.emit(id);
  }
  doDeleteRow(id: number) {
    this.deleteRow.emit(id);
  }
  doSortColumn(col: string) {
    this.sortCol.emit(col);
  }
  getSortClass(){
    return this.sortAscending ? 'sorted-asc' : 'sorted-des';
  }
}
