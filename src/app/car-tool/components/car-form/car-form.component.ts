import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Car } from "../../models/car.model";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-car-form",
  templateUrl: "./car-form.component.html",
  styleUrls: ["./car-form.component.css"]
})
export class CarFormComponent implements OnInit {
  @Input() buttonText = "Submit Car";
  @Input() cars: Car[];
  @Output() submitCar = new EventEmitter<{}>();
  carForm: FormGroup;
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.carForm = this.fb.group({
      id: "",
      make: "",
      model: "",
      year: "",
      hue: "",
      price: ""
    });
  }

  doSubmitCar() {
    const newCar: Car = {
      // ...this.carForm.value,
      id: Math.max(...this.cars.map(c => c.id), 0) + 1,
      make: this.carForm.value.make,
      model: this.carForm.value.model,
      year: this.carForm.value.year,
      color: this.carForm.value.hue,
      price: this.carForm.value.price
    };
    this.submitCar.emit(newCar);
    this.carForm.reset();
  }
}
