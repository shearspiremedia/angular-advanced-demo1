import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Car } from "../../models/car.model";

@Component({
  selector: ".app-car-row",
  templateUrl: "./car-row.component.html",
  styleUrls: ["./car-row.component.css"]
})
export class CarRowComponent implements OnInit {
  @Input() car: Car;
  @Output() deleteCar = new EventEmitter<{}>();
  @Output() editCar = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  doDeleteCar(id: number) {
    console.log("deleting " + id);
    this.deleteCar.emit(id);
  }
  doEditCar(id: number){
    this.editCar.emit(id);
  }
}
