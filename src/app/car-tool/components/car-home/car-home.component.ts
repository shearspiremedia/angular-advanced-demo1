import { Component, OnInit, Input } from "@angular/core";
import { Car } from "../../models/car.model";
import { CarsService } from '../../services/cars.service.service';

@Component({
  selector: "app-car-home",
  templateUrl: "./car-home.component.html",
  styleUrls: ["./car-home.component.css"]
})
export class CarHomeComponent implements OnInit {
  @Input() headerText = "Great Car Tool";
  sortColName = 'id';
  sortAscending = true;
  cars: Car[] = [];
  editCarId = -1;

  constructor(private carsSvc: CarsService) {}

  ngOnInit(): void {
    // this.carsSvc.setDefaultCars();
    this.refreshCars();
  }

  get sortedCars() {
    return this.cars;
  }
  doSortColumn(column: string) {
    if (this.sortColName === column){
      this.sortAscending = !this.sortAscending;
    } else {
      this.sortAscending = true;
    }
    this.sortColName = column;
    this.carsSvc.setSort(this.sortColName, this.sortAscending);
    this.refreshCars();
  }
  refreshCars() {
    return this.carsSvc.all().then(cars => {
      this.cars = cars;
    });
  }
  doAddCar(car: Car) {
    this.carsSvc
    .append(car)
    .then(() => this.refreshCars());
    this.editCarId = -1;
  }
  doDeleteCar(id: number) {
    this.carsSvc
    .remove(id)
    .then(() => this.refreshCars());
    this.editCarId = -1;
  }
  doReplaceCar(car: Car) {
    console.log('doReplaceCar' + JSON.stringify(car));
    this.carsSvc
    .replace(car)
    .then(() => this.refreshCars());
    this.editCarId = -1;
  }
  doCancelCar() {
    this.editCarId = -1;
  }
  doEditCar(carId: number) {
    console.log("home with doEditCar received " + carId)
    this.editCarId = carId;
  }
}
