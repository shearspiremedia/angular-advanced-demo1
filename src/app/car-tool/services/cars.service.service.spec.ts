import { TestBed } from '@angular/core/testing';

import { Cars.ServiceService } from './cars.service.service';

describe('Cars.ServiceService', () => {
  let service: Cars.ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Cars.ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
