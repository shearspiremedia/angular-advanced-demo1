import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Car } from '../models/car.model'

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  dataUrl = "http://localhost:4250/cars";
  sortColumn = 'id';
  sortDirection = 'asc';

  constructor(private httpClient: HttpClient) { }

  all() {
    return this.httpClient.get<Car[]>(this.dataUrl + '?_sort=' + this.sortColumn + '&_order=' + this.sortDirection).toPromise();
  }
  append(car: Car) {
    return this.httpClient.post<Car>(this.dataUrl, car).toPromise();
  }
  replace(car: Car) {
    return this.httpClient.put<Car>(this.dataUrl + '/' + car.id, car).toPromise();
  }
  remove(carId: number) {
    return this.httpClient.delete<void>(this.dataUrl + '/' + carId).toPromise();   
  }
  sort(column: string, ascdesc: string) {
    return this.httpClient.get<Car[]>(this.dataUrl + '?_sort=' + column + '&_order=' + ascdesc).toPromise();
  }
  setSort(column: string, direction: boolean) {
    this.sortColumn = column;
    this.sortDirection = direction ? 'asc' : 'desc';
  }
  setDefaultCars() {
    const defaultCars: Car[] = [
        {
          "id": 1,
          "make": "Chevy",
          "model": "Cruz",
          "year": 2006,
          "color": "red",
          "price": 8000
        },
        {
          "make": "Buick",
          "model": "Century",
          "year": 1976,
          "color": "navy",
          "price": 200,
          "id": 2
        },
        {
          "id": 3,
          "make": "Porsche",
          "model": "911",
          "year": 2020,
          "color": "teal",
          "price": 99000
        },
        {
          "id": 4,
          "make": "Fiat",
          "model": "Sedan",
          "year": 1988,
          "color": "maroon",
          "price": 400
        },
        {
          "id": 5,
          "make": "Mazda",
          "model": "3",
          "year": 2014,
          "color": "orange",
          "price": 12000
        }
      ];
    this.all()
    .then(cars => {
      cars.forEach(car => this.remove(car.id));
      defaultCars.forEach(car => this.append(car));
    });
    
  }
}
