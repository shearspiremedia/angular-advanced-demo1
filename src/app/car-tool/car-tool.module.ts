import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from '@angular/common/http';
import { CarHomeComponent } from "./components/car-home/car-home.component";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { CarTableComponent } from "./components/car-table/car-table.component";
import { CarRowComponent } from "./components/car-row/car-row.component";
import { CarFormComponent } from "./components/car-form/car-form.component";
import { CarEditRowComponent } from './components/car-edit-row/car-edit-row.component';

@NgModule({
  declarations: [
    CarHomeComponent,
    CarTableComponent,
    CarRowComponent,
    CarFormComponent,
    CarEditRowComponent
  ],
  imports: [CommonModule, ReactiveFormsModule, HttpClientModule, SharedModule],
  exports: [
    CarHomeComponent,
    CarTableComponent,
    CarRowComponent,
    CarFormComponent
  ]
})
export class CarToolModule {}
