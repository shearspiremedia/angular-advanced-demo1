import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-color-home",
  templateUrl: "./color-home.component.html",
  styleUrls: ["./color-home.component.css"]
})
export class ColorHomeComponent implements OnInit {
  headerText = "Color Tool";
  colors = ["aqua", "lime", "fuchsia", "red"];
  // aqua, black, blue, fuchsia, gray, grey, green,
  // lime, maroon, navy, olive, purple, red, silver, teal, white and yellow

  constructor() {}

  ngOnInit(): void {}

  doAddColor(color) {
    this.colors = this.colors.concat(color);
  }
}
